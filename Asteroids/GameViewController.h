//
//  GameViewController.h
//  Asteroids
//

//  Copyright (c) 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *loseScreen;

@end
