//
//  GameScene.h
//  Asteroids
//

//  Copyright (c) 2016 Corey Hom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene

-(void) passLoseScreen:(UIImageView*) loseScreen;

@end