//
//  GameScene.m
//  Asteroids
//
//  Created by Corey Hom on 2/17/16.
//  Copyright (c) 2016 Corey Hom. All rights reserved.
//

#import "GameScene.h"
@import CoreMotion;

@implementation GameScene

//Pass lose screen
UIImageView* loseScreen;

-(void) passLoseScreen: (UIImageView*) lose{
    loseScreen = lose;
}

//Working with the ship
CMMotionManager *motionManager;
SKSpriteNode *ship;

//Laser variables
NSMutableArray *shipLasers;
int nextShipLaser;

//Working with asteroids
NSMutableArray<SKSpriteNode *> *asteroidList;

//Adding lives and collisions?
int lives;

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    ship = [SKSpriteNode spriteNodeWithImageNamed: @"Spaceship"];
    ship.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    ship.xScale = 0.1;
    ship.yScale = 0.1;
    
    //move the ship using Sprite Kit's Physics Engine
    //Don't know if this works since I don't have an iPhone
    ship.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ship.frame.size];
    
    ship.physicsBody.dynamic = YES;
    
    ship.physicsBody.affectedByGravity = NO;
    
    ship.physicsBody.mass = 0.02;
    
    
    //Initialization of asteroids
    asteroidList = [[NSMutableArray alloc] initWithCapacity: 10];
    SKSpriteNode *asteroid1 = [SKSpriteNode spriteNodeWithImageNamed:@"Asteroid"];
    asteroid1.xScale = 1.0;
    asteroid1.yScale = 1.0;
    SKSpriteNode *asteroid2 = [SKSpriteNode spriteNodeWithImageNamed:@"Asteroid"];
    asteroid2.xScale = 1.0;
    asteroid2.yScale = 1.0;
    SKSpriteNode *asteroid3 = [SKSpriteNode spriteNodeWithImageNamed:@"Asteroid"];
    asteroid3.xScale = 1.0;
    asteroid3.yScale = 1.0;

    //apply force to space ship
    asteroid1.position = CGPointMake(CGRectGetMidX(self.frame)+100,CGRectGetMidY(self.frame)+200);
    asteroid2.position = CGPointMake(CGRectGetMidX(self.frame)-100,CGRectGetMidY(self.frame)-100);
    asteroid3.position = CGPointMake(CGRectGetMidX(self.frame)+100,CGRectGetMidY(self.frame));
    
    
    self.physicsWorld.gravity = CGVectorMake(0.0f, 0.0f);
    asteroid1.physicsBody = [SKPhysicsBody bodyWithTexture:asteroid1.texture size:asteroid1.texture.size];
    asteroid2.physicsBody = [SKPhysicsBody bodyWithTexture:asteroid2.texture size:asteroid1.texture.size];
    asteroid3.physicsBody = [SKPhysicsBody bodyWithTexture:asteroid3.texture size:asteroid1.texture.size];
    //Add the objects to the screen
    [self addChild: asteroid1];
    [self addChild: asteroid2];
    [self addChild: asteroid3];
    [self addChild: ship];
    [asteroidList addObject: asteroid1];
    [asteroidList addObject: asteroid2];
    [asteroidList addObject: asteroid3];
    NSLog(@"%lu",(unsigned long)asteroidList.count);
    
    //Make the initial asteroids move.
    
    
    //Create lasers for ship to use
    shipLasers = [[NSMutableArray alloc] initWithCapacity:3];
    for (int i = 0; i < 3; ++i) {
        SKSpriteNode *shipLaser = [SKSpriteNode spriteNodeWithImageNamed:@"Laser"];
        shipLaser.hidden = YES;
        [shipLasers addObject:shipLaser];
        [self addChild:shipLaser];
    }
    
    
    [self updateShipPositionFromMotionManager];
    [self startTheGame];
}


//Following an example of starting the game
- (void)startTheGame
{
    //get lives
    lives = 3;
    ship.hidden = NO;
    //reset ship position for new game
    ship.position = CGPointMake(self.frame.size.width * 0.1, CGRectGetMidY(self.frame));
    
    //setup to handle accelerometer readings using CoreMotion Framework
    [self startMonitoringAcceleration];
    
    for (SKSpriteNode *laser in shipLasers) {
        laser.hidden = YES;
    }
    
}


//Want to rotate ship using motion
- (void)startMonitoringAcceleration
{
    if (motionManager.accelerometerAvailable) {
        [motionManager startAccelerometerUpdates];
        NSLog(@"accelerometer updates on...");
    }
}

- (void)stopMonitoringAcceleration
{
    if (motionManager.accelerometerAvailable && motionManager.accelerometerActive) {
        [motionManager stopAccelerometerUpdates];
        NSLog(@"accelerometer updates off...");
    }
}

- (void)updateShipPositionFromMotionManager
{
    CMAccelerometerData* data = motionManager.accelerometerData;
    if (fabs(data.acceleration.x) > 0.2) {
        NSLog(@"acceleration value = %f",data.acceleration.x);
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */

    // grab the spaceship rotation and add M_PI_2
    CGFloat spaceShipRotation = ship.zRotation;
    float calcRotation = (float) (spaceShipRotation) + (float) (M_PI_2);
    
    // cosf and sinf use a Float and return a Float
    // however CGVector need CGFloat
    CGFloat intensity = 10.0;
    CGFloat xv = intensity * (CGFloat) (cosf(calcRotation));
    CGFloat yv = intensity * (CGFloat) (sinf(calcRotation));
    CGVector vector = CGVectorMake(xv, yv);
    
    //apply force to space ship
    [ship.physicsBody applyForce:vector];
    
    
    //Shoot 1/3 lasers
    SKSpriteNode *shipLaser = [shipLasers objectAtIndex:nextShipLaser];
    nextShipLaser++;
    if (nextShipLaser >= shipLasers.count) {
        nextShipLaser = 0;
    }
    
    //Set position of laser to ship
    shipLaser.position = CGPointMake(ship.position.x+shipLaser.size.width/2,ship.position.y+0);
    shipLaser.hidden = NO;
    [shipLaser removeAllActions];
    
    //Make laser move
    CGPoint location = CGPointMake(self.frame.size.width, ship.position.y);
    SKAction *laserMoveAction = [SKAction moveTo:location duration:1.0];
    //Make laser hidden when it hits something
    SKAction *laserDoneAction = [SKAction runBlock:(dispatch_block_t)^() {
        //NSLog(@"Animation Completed");
        shipLaser.hidden = YES;
    }];
    
    //Defines the sequence of actions
    SKAction *moveLaserActionWithDone = [SKAction sequence:@[laserMoveAction,laserDoneAction]];
    //Run it on the laser sprite
    [shipLaser runAction:moveLaserActionWithDone withKey:@"laserFired"];
}


//Called before each frame is rendered
-(void)update:(CFTimeInterval)currentTime {
    [self updateShipPositionFromMotionManager];
    
    //Update asteroid movement
    for (int i = 0; i < asteroidList.count; i++)
    {
        CGPoint shipPoint = CGPointMake(ship.frame.origin.x, ship.frame.origin.y);
        SKAction *moveAction = [SKAction moveTo:shipPoint duration:100.0];
        [asteroidList[i] runAction: moveAction];
    }
    
    
    //Handle collision with asteroids, lasers, and ship
    for (SKSpriteNode *asteroid in asteroidList) {
        if (asteroid.hidden) {
            continue;
        }
        for (SKSpriteNode *shipLaser in shipLasers) {
            if (shipLaser.hidden) {
                continue;
            }
            
            if ([shipLaser intersectsNode:asteroid]) {
                shipLaser.hidden = YES;
                asteroid.hidden = YES;
                
                NSLog(@"Asteroid destroyed.");
                continue;
            }
        }
        if ([ship intersectsNode:asteroid]) {
            asteroid.hidden = YES;
            SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                                   [SKAction fadeInWithDuration:0.1]]];
            SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
            [ship runAction:blinkForTime];
            lives--;
            NSLog(@"your ship has been hit!");
        }
    }
    //Short display lose screen when lives are 0
    if (lives == 0)
    {
        NSLog(@"You lose");
        loseScreen.hidden = NO;
    }
}

@end